module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Send data Widget',
    afterConstruct: function (self) {
        self.addRoutes();
    },
    construct: function (self, options) {
        self.addRoutes = function() {
            self.apos.app.post('/data', function(req, res) {
                return res;
            });
        };
    }
}